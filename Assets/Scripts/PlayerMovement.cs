﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;



public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float jumpSpeed;
    public float gravity;
    public Text winText;
    public Text returnText;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;

    void Start()
    {
        controller = GetComponent<CharacterController>();

        // Spawns player at start
        gameObject.transform.position = new Vector3(0, 2, -4);
        winText.text = "";
        returnText.text = "";
    }

    void Update()
    {
        if (controller.isGrounded)
        {
            

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

       
        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);

        
        controller.Move(moveDirection * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Finish"))
        {
            winText.text = "Rescue is on the way!";
            returnText.text = "Returning to menu in 5 seconds.";
            StartCoroutine(BackToMenu());

        }

        if(other.gameObject.CompareTag("Fake"))
        {

            other.gameObject.SetActive(false);
        }

    }

    IEnumerator BackToMenu()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(0);

    }
}
